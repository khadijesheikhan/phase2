from django.conf.urls import patterns, include, url
from django.views.static import serve
import final.settings as settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'final.views.home', name='home'),
    url(r'^shop/', include('shop.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', serve,{
        'document_root': settings.MEDIA_ROOT,
    })

)

urlpatterns += patterns('',
    url(r'^captcha/', include('captcha.urls')),
)
