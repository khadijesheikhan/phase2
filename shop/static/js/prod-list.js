var page_num = 1;
var page_total=5;
var cat_id = $("#cat-id").attr("value");
var str = document.getElementById("query").value;
var products;
var reset = 0;

if(str)
    searchs();
else
    f(cat_id);


function searchs(){
    str = document.getElementById("query").value;
    page_num = 1;
    reset=2;
    searchProds();

    window.history.pushState(
        {'cat_id': cat_id, 'str': str, 'page_num': page_num, 'page_total': page_total, 'products': products, 'reset': reset }
        ,"","/shop/products/q="+str+"/");
}

function f(id){
    cat_id=id;
    page_num = 1;
    reset=1;
    loadProds();

    window.history.pushState(
        {'cat_id': cat_id, 'str': str, 'page_num': page_num, 'page_total': page_total, 'products': products, 'reset': reset}
        ,"","/shop/products/"+cat_id+"/");
}

function searchProds(){
    var ajaxData = {
        query: str,
        page: page_num
    };
    ajaxx(ajaxData);
}

function ajaxx(ajaxData){
    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    $("#prods0").html("");
    $("#prods1").html("");
    $(".products-title").html("");

    $.ajax({
        url: '/shop/updateList/',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            page_total = parseInt(data.size);
            $("title").text(data.title);
            $(".products-title").text("» "+data.title+"...");
            products = data.products;
            document.getElementById("prods0").innerHTML="";
            document.getElementById("prods1").innerHTML="";
            paginate();
            for(var i in products){
                prod = products[i];
                $("#sample-prod h5").html(prod.name);
                if(prod.isOff)
                            $("#sample-prod .price").html("<del><span>"+prod.price+"</span></del> <strong style='color: red'><span>"+prod.realPrice+"</span><span> تومان</span></strong>");
                else

                            $("#sample-prod .price").html("<span>قیمت: </span><span>"+prod.price+"</span><span> تومان</span>");
                $("#sample-prod .namelink").attr('href', '/shop/product/'+prod.id);
                $("#sample-prod .imglink").attr('href', '/shop/product/'+prod.id);
                $("#sample-prod img").attr('src', prod.url);
                $("#sample-prod img").attr('class', 'thumbnail');
                $("#sample-prod .average-rate").rating({symbol: "★", readonly: true, showClear: false, showCaption: false, rtl:true, size: 'xs' });
                $("#sample-prod .average-rate").rating('update', prod.rate);
                console.log($("#sample-prod .average-rate").html());
                s = $("#sample-prod ").html();
                var newProd = document.createElement('div');
                newProd.id="prodNum"+prod.id;
                newProd.className="col-lg-2";
                newProd.innerHTML=s;
                document.getElementById("prods"+parseInt(i/6)).appendChild(newProd);
                addOnClick(prod);
            }
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function loadProds(){
    $("#query").val('');
    var ajaxData = {
        cat: cat_id,
        page: page_num
    };
    ajaxx(ajaxData);
}

function addOnClick(prod){
    $("#prodNum"+prod.id +" button").click(
        function(){
            addToCart(prod.id, prod.name, prod.price);
        }
    );
}

window.onpopstate = function(event) {
    var state = event.state;

    if (state) {
        tmp = state.reset;
        reset = 0;
        cat_id=state.cat_id;
        str = state.str;
        page_num = state.page_num;
        page_total=state.page_total;
        products = state.products;
        if(tmp==1)
            loadProds();
        if(tmp==2){
             document.getElementById("query").value = str;
            console.log(str);
            searchProds();
        }
    }
    else{
        if(reset==0){
            if(cat_id!=""){
                reset = 1; //loadProds
                f(cat_id);
            }
            else{
                reset = 2; //searchProds
                searchs();
            }
        }
        else
            return;
    }
}

function page_change(i){
    var temp = page_num;
    if(i==0){
        if(page_num>1)
            page_num--;
    }
    else if(i>page_total){
        if(page_num<page_total)
            page_num++;
    }
    else
        page_num=i;
    if(reset==1)
        loadProds();
    if(reset==2)
        searchProds();
    if(page_num!=temp){
        refreshPagination();
        var url="/shop/products/"+cat_id+"/";
        if(reset==2)
            url="/shop/products/q="+str+"/";
        window.history.pushState(
        {'cat_id': cat_id, 'str': str, 'page_num': page_num, 'page_total': page_total, 'products': products , 'reset': reset}
        ,"",url);
    }
}

function refreshPagination(){
    if(page_num==1)
        $("#p0").addClass("disabled");
    else
        $("#p0").removeClass("disabled");
    if(page_num==page_total){
        $("#p"+(parseInt(page_total)+1)).addClass("disabled");
    }
    else
        $("#p"+(parseInt(page_total)+1)).removeClass("disabled");
    if(page_num>=1 && page_num<=page_total){
        for(var i=1; i<=page_total; ++i)
            $("#p"+i).removeClass("active");
    }
    $("#p"+page_num).addClass("active");
}

function paginate(){
    $(".pagination").html("");
    if(page_total>1){
        $(".pagination").append(
                "<li id='p0'><a onclick=page_change(0)>«</a></li>\n<li id='p1'><a onclick=page_change(1)>1</a></li>");
        for(var i=2; i<=page_total; i++){
            $(".pagination").append(
                "<li id='p"+i+"'><a onclick=page_change("+i+")>"+i+"</a></li>");
        }
        $(".pagination").append(
                "<li id='p"+(parseInt(page_total)+1)+"'><a onclick=page_change("+(parseInt(page_total)+1)+")>»</a></li>");
    }
    refreshPagination();
}