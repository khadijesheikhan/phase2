var cartSize=0;
var off=0;
console.log("cart.js=");
window.onload = function(){

	if(typeof(Storage)!=="undefined" && localStorage.prodNames!=undefined){
		var names = JSON.parse(localStorage.prodNames);
		var nums = JSON.parse(localStorage.prodNums);
		var offs = JSON.parse(localStorage.offsets);
		var prices = JSON.parse(localStorage.prodPrices);
        cartSize = localStorage.cartSize;
        off = localStorage.off;
		for(var i=cartSize-1; i>=0; --i)
            createCartProd(nums[i], offs[i], names[i], prices[i]);
	}
};

function saveToLocalStorage(){
	if(typeof(Storage)!=="undefined"){
		var prodNums =[], offsets=[], prodNames=[], prodPrices=[];
		$("#cart").children().each(function(){

            prodNums.push($(this).data('pid'));
            offsets.push($(this).data('offset'));
            prodNames.push($(this).data('pname'));
            prodPrices.push($(this).data('pprice'));
        });
		localStorage.prodNums = JSON.stringify(prodNums);
		localStorage.prodNames = JSON.stringify(prodNames);
		localStorage.offsets = JSON.stringify(offsets);
		localStorage.prodPrices = JSON.stringify(prodPrices);
        localStorage.cartSize = cartSize;
        localStorage.off = off;

	}
}

function addToCart(id, name, price){
    cartSize++;
    off++;
    createCartProd(id, off, name, price);
    saveToLocalStorage();
}

function createCartProd(prodNum, offset, prodName, prodPrice){
    $("#price").html(parseInt($("#price").html(), 10)+parseInt(prodPrice, 10));
    $("#cart-prod-sample .breadcrumb").attr("id", "cart-prod"+offset);
    $("#cart-prod"+offset).attr("data-pid", prodNum);
    $("#cart-prod"+offset).attr("data-offset", offset);
    $("#cart-prod"+offset).attr("data-pname", prodName);
    $("#cart-prod"+offset).attr("data-pprice", prodPrice);

    $("#cart-prod"+offset+" li").html(prodName);
    var x = $("#cart-prod-sample");
    $("#cart").prepend(x.html());
    $("#cart-prod"+offset+" .close").click(function(){
        $("#price").html(parseInt($("#price").html(), 10)-parseInt(prodPrice, 10));
        $("#cart-prod"+offset).remove();
        cartSize--;
        saveToLocalStorage();
    });
}
