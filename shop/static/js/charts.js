$('#dateview').click(function(e) {
    $("#prod-list").hide();
    $("#charts").hide();
    $("#date-list").show();
});
var sent = false;
var s1, ticks, s2;
$('#chartview').click(function(e) {
    $("#prod-list").hide();
    $("#charts").show();
    $("#date-list").hide();
    if(sent)
        draw();
    else{
        chart();
        sent = true;
    }
});

$('#prodview').click(function(e) {
    $("#prod-list").show();
    $("#charts").hide();
    $("#date-list").hide();
});


function draw(){
       $.jqplot('chart1', [s1], {
                // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                animate: !$.jqplot.use_excanvas,
                title: '<h3>تعداد فروش هر محصول</h3>',
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    pointLabels: { show: true }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    }
                },
                highlighter: { show: false }
            });

            $.jqplot('chart2', [s2], {
                // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                animate: !$.jqplot.use_excanvas,
                title: '<h3>سود فروش محصولات</p>',
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    pointLabels: { show: true }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    }
                },
                highlighter: { show: false }
            });


}

function chart(){
    $.ajax({
        url: '/shop/chartdata/',
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            $.jqplot.config.enablePlugins = true;
            s1 = data.nums;
            s2 = data.sood;
            ticks = data.names;
            console.log("here");
            draw();
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}