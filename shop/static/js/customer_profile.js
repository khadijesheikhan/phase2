function get_user_info(mode)
{
    var userinfo;
    $('#loading').show();

    $.ajax({
        url: '/shop/userinfo/',
        data: {},
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            if (mode == 0)
                load_profile(data);
            else
                reset_profile_form(data);
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
            return null;
        }
    });

    return userinfo;
}

function dummy()
{
    alert('hi');
}

function change_password()
{
    var form = $('#password_form').serializeArray();
    clear_password_errors();
    $('#loading').show();

    $.ajax({
        url: '/shop/change/password/',
        data: form,
        type: 'POST',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            console.log(data.message);
            data.errors.pass_conf = data.errors.__all__;
            password_message(data.message, data.status);
            password_errors(data.errors);
            glob = data;
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function password_errors(errors){
    for (field in errors)
    {
        console.log('errors for ' + field + ' are ' + errors['field']);
        error = errors[field];
        var error_field = '<ul>';
        for (i in error)
        {
            console.log(error[i]);
            error_field += "<li>" + error[i] + "</li>";
        }
        error_field += '</ul>'
        $('#' + field + ' .error-list').html(error_field);
    }
}
function password_message(message, status){
    field_val = '<div id="password_form_message" class="alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">×</button>';
    field = $('#password_message');
    field_val += message;
    field_val += "</div>"
    field.html(field_val);

    if (status =='fail')
    {
        $('#password_form_message').removeClass();
        $('#password_form_message').addClass('alert alert-dismissable alert-danger');
    }
    else
    {
        $('#password_form_message').removeClass();
        $('#password_form_message').addClass('alert alert-dismissable alert-success')
    }
}
function clear_password_errors(){
    $('#password_form .error-list').html('');
    $('#password_message').html('');
}
function clear_password_fields(){
    $('#password_form input').val('');
}

function edit_profile()
{
    console.log('WE HAVE COME!');
    var form = $('#profile_form').serializeArray();
    clear_profile_errors();
    $('#loading').show();

    $.ajax({
        url: '/shop/edit/profile/',
        data: form,
        type: 'POST',
        dataType: 'json',

        success: function(data, success){
            console.log('Success!');
            console.log(data);
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            profile_errors(data.errors);
            profile_message(data.message, data.status);
            get_user_info(0);
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function profile_errors(errors){
    for (field in errors)
    {
        error = errors[field];
        var error_field = '<ul>';
        for (i in error)
        {
            console.log(error[i]);
            error_field += "<li>" + error[i] + "</li>";
        }
        error_field += '</ul>'
        $('#' + field + ' .error-list').html(error_field);
    }
}
function profile_message(message, status){
    field_val = '<div id="profile_form_message" class="alert alert-dismissable"><button type="button" class="close" data-dismiss="alert">×</button>';
    field = $('#profile_message');
    field_val += message;
    field_val += "</div>"
    field.html(field_val);

    if (status =='fail')
    {
        $('#profile_form_message').removeClass();
        $('#profile_form_message').addClass('alert alert-dismissable alert-danger');
    }
    else
    {
        $('#profile_form_message').removeClass();
        $('#profile_form_message').addClass('alert alert-dismissable alert-success')
    }
}
function clear_profile_errors(){
    $('#profile_form .error-list').html('');
    $('#profile_message').html('');
}
function load_profile(userinfo){
    console.log(userinfo);

    if (userinfo == null)
        return;

    for (field in userinfo)
        $('#user_' + field).html(userinfo[field]);
}
function reset_profile_form(userinfo){
    console.log(userinfo);

    if (userinfo == null)
        return;

    for (field in userinfo)
        $('#id_' + field).val(userinfo[field]);
}

