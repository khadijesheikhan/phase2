$(".average-rate").each(function(){
    $(this).rating({symbol: "★", readonly: true, showClear: false, showCaption: false, rtl:true, size: 'xs' });
    $(this).rating('update', $(this).attr("data-rate"));
});