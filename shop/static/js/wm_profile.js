var glob;

function addProd(e)
{
    inp = e.target.parentNode.parentNode.parentNode.getElementsByTagName('input')[0];

    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    var ajaxData = {
        pk: inp.id.substr(4),
        val: inp.value,
        type: 'add'
    }

    target = inp.parentNode.parentNode.parentNode.children[2];

    $.ajax({
        url: '/shop/addRemoveProduct',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data,success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            target.innerHTML = "تعداد : " + data.new_num;
            inp.value = "";
        },
        error: function(xhrs,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function minProd(e)
{
    inp = e.target.parentNode.parentNode.parentNode.getElementsByTagName('input')[0];

    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    var ajaxData = {
        pk: inp.id.substr(4),
        val: inp.value,
        type: 'remove'
    }

    target = inp.parentNode.parentNode.parentNode.children[2];

    $.ajax({
        url: '/shop/addRemoveProduct',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data,success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            target.innerHTML = "تعداد : " + data.new_num;
            inp.value = "";
        },
        error: function(xhrs,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function accBill(e)
{
    id = e.target.id.substr(7);

    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    ajaxData = {
        'id': id,
        'type': 'accept'
    };

    target = e.target.parentNode.parentNode.parentNode.parentNode;

    $.ajax({
        url: '/shop/billManagement',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data,success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            target.hidden = true;
            js = $(target);
            name = js.find('.col-md-4:first-child').html();
            time = js.find('.col-md-4:nth-child(2)').html();
            body = js.find('.panel-body').html();
            pk_id = js.find('a:last').attr('href').split('collapse')[1];
            js.remove();
            dest = $('#rej-rec-tmplt').clone();
            dest.find('.col-md-3').html(name);
            dest.find('.col-md-4').html(time);
            dest.find('.panel-body').html(body);
            cur_href = dest.find('.col-md-5 a').attr('href');
            dest.find('.col-md-5 a').attr('href',cur_href+pk_id);
            dest.find('#recBill').attr('id','recBill'+pk_id);
            dest.find('#rejBill').attr('id','rejBill'+pk_id);
            dest.find('.panel-collapse').attr('id', 'collapse'+pk_id);

            dest.appendTo('#accordion2');
            dest.removeAttr('hidden');
            dest.removeAttr('id');
            dest.show();
        },
        error: function(xhrs,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function recBill(e)
{
    id = e.target.id.substr(7);

    glob = e.target;

    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    ajaxData = {
        'id': id,
        'type': 'receive'
    };

    target = e.target.parentNode.parentNode.parentNode.parentNode;

    $.ajax({
        url: '/shop/billManagement',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data,success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            target.hidden = true;
        },
        error: function(xhrs,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function rejBill(e)
{
    id = e.target.id.substr(7);

    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    ajaxData = {
        'id': id,
        'type': 'reject'
    };

    target = e.target.parentNode.parentNode.parentNode.parentNode;

    $.ajax({
        url: '/shop/billManagement',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data,success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            target.hidden = true;
            js = $(target);
            name = js.find('.col-md-3').html();
            time = js.find('.col-md-4').html();
            body = js.find('.panel-body').html();
            pk_id = js.find('a:last').attr('href').split('collapse')[1];
            js.remove();
            dest = $('#rej-tmplt').clone();
            dest.find('.col-md-4').html(name);
            dest.find('.col-md-6').html(time);
            dest.find('.panel-body').html(body);
            dest.find('.col-md-1 a').attr('href','#collapse'+pk_id);
            dest.find('.panel-collapse').attr('id','collapse'+pk_id);

            dest.appendTo('#accordion3');
            dest.removeAttr('hidden');
            dest.removeAttr('id');
            dest.show();
        },
        error: function(xhrs,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}