commentpage=0;
pagesize=5;
showComments();

function showComments(){
    num = Math.min(parseInt($("#commentNum").html()), (commentpage+1)*pagesize);
    for(var i=commentpage*pagesize; i<num; i++){
        $("#comment"+i).show();
        console.log("#comment"+i);
    }
    commentpage++;
    if(parseInt($("#commentNum").html())<commentpage*pagesize){
        $("#showMore").hide();
    }
}


$("#addToCart").click(
    function(){
        addToCart($(".prod-thumb").data("id"), $(".prod-thumb").data("name"), $(".prod-thumb").data("price"));

    }
);

$("#logintoadd").click(
    function(){
        location.href="/shop/login/?next="+location.href;
    }
);

function addComment(name, comment, time){

    var s1 ="<div class='panel panel-success'><div class='panel-heading'>";
    var ss="<span class='pull-left'>"
    var s2 ="</span></div><div class='panel-body'>";
    var s3 ="</div></div>";
    $(s1+name+ss+time+s2+comment+s3).insertAfter("#newcomment");
}

function addNewComment() {
    console.log($("#product").attr("data-id"));
    comment = $("#commentInput").val();
    ajaxData={
        'id': $("#product").attr("data-id"),
        'body': comment
    };
    $.ajax({
        url: '/shop/addComment/',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);

            addComment(data.name, comment, data.time);
            $("#commentInput").val('');
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

$(document).ready( function(){
    h = Math.max($(".prod-detail").height(), $(".prod-thumb").height());
    $(".prod-detail").height(h);
    $(".prod-thumb").height(h);
});