/**
 * Created by Phoenix on 8/28/14.
 */

options={symbol: "★", readonly: true, showClear: false, showCaption: false, rtl:true, size: 'md', step: 1};
$("#average-rate").rating(options);

options={symbol: "★", step: 1, starCaptions: function(val) {return 'امتیازدهی شما';},
    starCaptionClasses: function(val) {if(val==0) return 'label label-danger'; else return 'label label-success';},
    clearCaption: "بدون امتیازدهی"
}
$("#user-rate").rating(options);


$('#user-rate').on('rating.change', function(event, value, caption) {
    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    var ajaxData = {
        prod: $(".prod-thumb").data("id"),
        rate: value
    };
    $.ajax({
        url: '/shop/rateUpdate/',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            $("#average-rate").rating('update', data.newRate);
            $("#rate").html(data.newRate);
            $("#rateNum").html(data.rateNum);
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
});

$('#user-rate').on('rating.clear', function(event, value, caption) {
    $("#error").hide();
    $("#done").hide();
    $("#loading").show();

    var ajaxData = {
        prod: $(".prod-thumb").data("id"),
        rate: 0
    };
    $.ajax({
        url: '/shop/rateUpdate/',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            $("#average-rate").rating('update', data.newRate);
            $("#rate").html(data.newRate);
            $("#rateNum").html(data.rateNum);
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
});
