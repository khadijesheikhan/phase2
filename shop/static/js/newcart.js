/**
 * Created by newbornPhoenix on 1/23/14.
 */

console.log("newcart.js ");
window.onload = function(){
    loadCart();
};

function addToCart(id, name, price){
    ajaxData={
        'id': id
    };
    $.ajax({
        url: '/shop/addToCart/',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            createCartProd(id, data.buy_id, name, price);
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function loadCart(){
    $.ajax({
        url: '/shop/loadCart/',
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);
            cart = data.cart;
    		for(var i=0; i<cart.length; ++i){
                createCartProd(cart[i].prod_id, cart[i].buy_id, cart[i].name, cart[i].realPrice);
            }

            $("#billing-prod-list").html($("#cart").html());
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}

function createCartProd(prodNum, offset, prodName, prodPrice){
    $("#price").html(parseInt($("#price").html(), 10)+parseInt(prodPrice, 10));
    $("#cart-prod-sample .breadcrumb").attr("id", "cart-prod"+offset);
    $("#cart-prod"+offset).attr("data-pid", prodNum);
    $("#cart-prod"+offset).attr("data-offset", offset);
    $("#cart-prod"+offset).attr("data-pname", prodName);
    $("#cart-prod"+offset).attr("data-pprice", prodPrice);
    $("#cart-prod"+offset+" .active").html(prodName+" ("+prodPrice+" تومان)");
    $("#cart-prod"+offset+" .active").attr("href", "/shop/product/"+prodNum);
    var x = $("#cart-prod-sample");
    $("#cart").prepend(x.html());
    console.log("removeFromCart("+offset+","+ prodPrice+")");
    $("#cart-prod"+offset+" .close").attr("onclick", "removeFromCart("+offset+","+ prodPrice+")");
}

function removeFromCart(offset, prodPrice){
    ajaxData={
        'buy_id': offset
    };
    $.ajax({
        url: '/shop/removeFromCart/',
        data: ajaxData,
        type: 'GET',
        dataType: 'json',

        success: function(data, success){
            $("#loading").hide();
            $("#error").hide();
            $("#done").show();
            setTimeout(function(){
            $("#done").fadeOut();},1000);

            $("#price").html(parseInt($("#price").html(), 10)-parseInt(prodPrice, 10));
            $("#cart-prod"+offset).remove();

            $("#billing-prod-list").html($("#cart").html());
            $(".tot-price").html($("#price").html());
        },
        error: function(xhr,status){
            $("#loading").hide();
            $("#error").show();
            $("#done").hide();
        }
    });
}