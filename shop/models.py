from django.db import models
from datetime import datetime
import re
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db.models.aggregates import Avg


class Category(models.Model):
    name = models.CharField(max_length=30)
    sup_cat = models.ForeignKey("self", blank=True, null=True,
                                limit_choices_to={'sup_cat': None}
                                )

    def __str__(self):
        if self.sup_cat is None:
            return self.name
        return self.sup_cat.name + " >> " + self.name


class MyMgr(BaseUserManager):
    def create_user(self, username, password, name):
        user = self.model(username=username, name=name)
        user.set_password(password)
        user.is_active = True

        user.save(using=self._db)
        return user

    def create_customer(self, username, password, name, email, address, tel):
        user = self.create_user(username, password, name)
        user.status='CU'
        user.email = email
        user.address = address
        user.tel = tel
        user.save(using=self._db)
        bill = Bill(state=0, customer=user)
        bill.save()
        return user

    def create_superuser(self, username, password, name):
                u = self.create_user(username, password, name)
                u.is_staff = True
                u.is_superuser = True
                u.save(using=self._db)
                return u

USER_TYPE_CHOICES = (
    ('GM', 'GeneralManager'),
    ('SHM', 'ShelfManager'),
    ('WM', 'WarehouseManager'),
    ('CU', 'Customer'),
)

BILL_STATUS_CHOICES = (
    ( 0, 'سبد خرید'),
    ( 1, 'در حال بررسی'),
    ( 2, 'در حال ارسال'),
    ( 3, 'تحویل داده شده'),
    ( 4, 'بازگردانده شده'),
)

class User(AbstractBaseUser, PermissionsMixin):
    is_staff = models.BooleanField(default=False)
    status = models.CharField(max_length=3, choices=USER_TYPE_CHOICES)

    username = models.CharField(max_length=40, unique=True)

    name = models.CharField(max_length=40)
    email = models.EmailField(blank=True, null=True)
    address = models.CharField(max_length=150, blank=True, null=True)
    tel = models.IntegerField(blank=True, null=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['name']
    objects = MyMgr()

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return self.username

    def total_spent(self):
        ans = 0
        for bill in self.bill_set.filter(state=3):
            ans += bill.sum()
        return ans

    def total_order(self):
        ans = 0
        for bill in self.bill_set.filter(state=3):
            ans += len(bill.buy_set.all())
        return ans

class Rate(models.Model):
    product = models.ForeignKey('Product')
    owner = models.ForeignKey('User')
    rate = models.IntegerField()

    class Meta:
        unique_together = (("product", "owner"),)

    def __str__(self):
        return self.owner.name+" درباره "+ self.product.name

class Product(models.Model):
    name = models.CharField(max_length=30)
    creator = models.CharField(max_length=30)
    publisher = models.CharField(max_length=30)
    category = models.ForeignKey(Category)
    price = models.PositiveIntegerField()
    availability = models.BooleanField(default=True)
    number = models.IntegerField(default=0)
    sold = models.IntegerField(default=0)
    offPercent = models.IntegerField(default=0)
    description = models.TextField(max_length=256)
    small_image = models.FileField(upload_to='images/thmb')
    time = models.DateTimeField(default=datetime.now())

    class Meta:
        ordering = ('-time',)

    def __str__(self):
        return self.name

    def match(self, s):
        print(s, self.name, self.category)
        return re.match(s, self.name) or re.match(s, self.category)

    def real_value(self):
        return (100-self.offPercent)/100*self.price

    def total_order(self):
        return len(Product.objects.filter(pk=self.pk).filter(buy__bill__state=3))

    def total_bought(self):
        return self.real_value() * self.total_order()

    def rate(self):
        return Rate.objects.filter(product=self).aggregate(Avg('rate'))['rate__avg']

    def round_rate(self):
        return round(self.rate()*2)/2


class Bill(models.Model):
    state = models.IntegerField(choices=BILL_STATUS_CHOICES)
    customer = models.ForeignKey(User, limit_choices_to={'status': 'CU'})
    spent = models.IntegerField(default=0)
    time = models.DateTimeField(default=datetime.now())

    def sum(self):
        ans = 0
        for x in self.buy_set.all():
            ans += (100.0-x.product.offPercent)/100*x.product.price
        return ans

    def __str__(self):
        return self.get_state_display()

    class Meta:
        ordering = ('-time',)



class Buy(models.Model):
    product = models.ForeignKey(Product)
    bill = models.ForeignKey(Bill)

    def __str__(self):
        return self.bill.customer.name+", "+self.product.name


class Comment(models.Model):
    owner = models.ForeignKey(User)
    context = models.CharField(max_length="150")
    product = models.ForeignKey(Product)
    time = models.DateTimeField(default=datetime.now())

    class Meta:
        ordering = ('-time',)

    def __str__(self):
        return self.owner.name+" "+self.product.name

