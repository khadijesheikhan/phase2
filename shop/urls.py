from django.conf.urls import patterns, url

from shop import views, view2

urlpatterns= patterns('shop.views',
    url(r'^$', views.index),
    url(r'^product/(?P<id>\d+)/$', views.product),
    url(r'^profile/$', views.profile),
    url(r'^products/$', views.products),
    url(r'^products/(?P<id>\d+)/$', views.products_with_id),
    url(r'^products/q=(?P<q>\S*)/$', views.products_with_q),
    url(r'^updateList/$', views.updateProductList),
    url(r'^login/$',views.login),
    url(r'^billing/$',views.billing),
    url(r'^payment/$',views.payment),
    url(r'^logout/$', views.logout),
    url(r'^loadCart/$', views.load_cart),
    url(r'^addToCart/$', views.add_to_cart),
    url(r'^addComment/$', views.add_comment),
    url(r'^removeFromCart/$', views.remove_from_cart),
    url(r'^edit/(?P<id>\d+)$', views.editProduct),
    url(r'^remove/(?P<id>\d+)$', views.removeProduct),
    url(r'^profile/newProduct', views.newProduct),
    url(r'^undoRemove/(?P<id>\d+)$', views.undoRemoveProduct),
    url(r'^edit/profile/$', views.edit_customer_details),
    url(r'^change/password/$', views.change_customer_password),
    url(r'^rateUpdate/$', views.rateUpdate),
    url(r'^addRemoveProduct/$', views.addRemoveProduct),
    url(r'^profile/addNewCategory$', views.addNewCategory),
    url(r'^profile/removeCategory$', views.removeCategory),
    url(r'^profile/selectCategory$', views.selectCategory),
    url(r'^profile/editCategory/(?P<id>\d+)$', views.editCategory),
    url(r'^billManagement/$', views.billManagement),
    url(r'^user/(?P<id>\d+)', views.user_with_id),

    url(r'^dateSearch/$', view2.dateSearch),
    url(r'^productSearch/$', view2.productSearch),
    url(r'^userinfo/$', views.get_user_info),

    url(r'^chartdata/$', view2.chartData),


    url(r'^chert/$', views.chert),
)