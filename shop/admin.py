from django.contrib import admin
from shop.models import Product, Category, Bill, Comment, Buy, User, Rate
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Bill)
admin.site.register(Comment)
admin.site.register(Buy)
admin.site.register(User)
admin.site.register(Rate)