import json
from django.db.models.query_utils import Q
from django.http.response import HttpResponse, Http404
from django.shortcuts import render
from django.template.base import Template
from django.template.context import Context
from django.template import loader
from shop.forms import DateSearchForm, ProductSearchForm, RemoveCatForm, ProductForm, NewCatForm
from shop.models import Buy, Product, Category, Bill

__author__ = 'newbornPhoenix'


def chartData(request):
    prods=Product.objects.all()
    names=[]
    nums=[]
    sood=[]
    for prod in prods:
        num = Buy.objects.filter(product=prod).count()
        names.append(prod.name)
        nums.append(num)
        sood.append(num*prod.price)

    return HttpResponse(json.dumps({'names': names, 'nums': nums, 'sood': sood}), content_type='aplication/json')

def dateSearch(request):
    if request.POST:
        form = DateSearchForm(request.POST)
        if form.is_valid():
            s = form.cleaned_data['start']
            t = form.cleaned_data['to']
            buys = Buy.objects.filter(Q(product__seller=request.user)& Q(bill__time__gte=s))
            content = loader.get_template('buys.html').render(Context({'buys': buys}))
            return HttpResponse(json.dumps({'x': content }), content_type='application/json')
    else:
        raise Http404()

def productSearch(request):
    print("salam")
    if request.GET:
        id = int(request.GET.get(u'id'))
        print(id)
        prod = request.user.product_set.all()[len(request.user.product_set.all())-id]
        print(prod)
        buys = Buy.objects.filter(Q(product=prod))
        content = loader.get_template('buys.html').render(Context({'buys': buys}))
        return HttpResponse(json.dumps({'x': content }), content_type='application/json')
    else:
        raise Http404()


