from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    args = '<>'
    help = 'Just for the sake of testing this weird feature'

    def handle(self, *args, **options):
        self.stdout.write("Relax, you're doing fine")
