import codecs
import os
import csv
import shop.models as models
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):

    args = '<>'
    help = 'Initializes the Database with initial data, pictures etc.'

    # GENRE

    genres = [
        u"کناب",
        u"سریال",
        u"فیلم",
        u"موسیقی",
        u"کتاب رمان",
        u"کتاب علمی",
        u"کتاب شعر",
        u"کتاب کودک و نوجوان",
        u"سریال خانوادگی",
        u"سریال کمدی",
        u"سریال علمی تخیلی",
        u"سریال جنایی",
        u"فیلم خانوادگی",
        u"فیلم کودک و نوجوان",
        u"فیلم جنگی",
        u"فیلم مستند",
        u"موسیقی سنتی",
        u"موسیقی پاپ",
        u"موسیقی راک",
        u"موسیقی متال"
    ]

    # PRODUCTS

    # SLIDE SHOW

    def handle(self, *args, **options):

        # initializing genres

        i = 0

        #self.stdout.write ('kire avval')
        for s in self.genres:
            #self.stdout.write ('kire dovom')
            i += 1
            a = s.split(u" ")
            if len(a) == 1:
                c = models.Category(name=a[0], sup_cat=None)
                self.stdout.write('kir')
                c.save()
            else:
                self.stdout.write(a[0])
                super = models.Category.objects.get(name=a[0])
                a.remove(a[0])
                c = models.Category(name=u' '.join(a), sup_cat=super)


