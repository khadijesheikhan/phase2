from django.forms.extras.widgets import SelectDateWidget
from shop.models import Product, Comment, User, Category
from django import forms
from django.utils.translation import gettext as _
import datetime


class DateSearchForm(forms.Form):
    start = forms.DateField(widget=SelectDateWidget(), initial=datetime.date.today())
    to = forms.DateField(widget=SelectDateWidget(), initial=datetime.date.today())

class ProductSearchForm(forms.Form):
    product = forms.ModelChoiceField(queryset=Product.objects.none())

    def __init__(self, user, *args, **kwargs):
        super(ProductSearchForm, self).__init__(*args, **kwargs)
        self.fields['product'] = forms.ModelChoiceField(queryset=Product.objects.all())


class RemoveCatForm(forms.Form):
    cat = forms.ModelChoiceField(label= _(u'نام دسته'), queryset=Category.objects.all());#filter(sup_cat__isnull=True).order_by('name')),


class NewCatForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('name', 'sup_cat')
        labels = {
            'name': _(u'نام دسته'),
            'sup_cat': _(u'دسته‌ی بالاتر'),
        }

class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = ('name', 'creator', 'category', 'price', 'offPercent', 'description', 'small_image')
        labels = {
            'name': _(u'نام محصول'),
            'creator': _(u'پدید آورنده'),
            'category': _(u'دسته'),
            'price': _(u'قیمت'),
            'offPercent': _(u'درصد تخفیف'),
            'description': _(u'توضیحات'),
            'small_image': _(u'تصویر محصول'),
        }
        error_messages = {
            'name': {
                'max_length': _(u'نام محصول بیش از حد بلند است.'),
                'required': _(u'لطفا نام محصول را وارد نمایید.'),
            },
            'creator': {
                'max_length': _(u'نام پدید آورنده بیش از حد بلند است'),
                'required': _(u'لطفا نام پدید آورنده را وارد نمایید'),
            },
            'category':{
                'required': _(u'لطفا دسته بندی محصول را انتخاب نمایید.'),
            },
            'price':{
                'required': _(u'لطفا قیمت محصول را وارد نمایید'),
            },
            'offPercent':{
                'required': _(u'لطفا درصد تخفیف را وارد نمایید'),
            },
            'description': {
                'required': _(u'لطفا توضیحات مربوط به محصول را وارد نمایید'),
            },
            'small_image': {
                'required': _(u'لطفا عکس مرتبط با محصول را انتخاب نمایید.'),
            },
        }

        widgets = {
            'description': forms.Textarea(attrs={'cols':'32', 'rows':'5'}),
        }


class CustomerSignUpForm (forms.Form):

    username = forms.CharField(label=u'نام کاربری',max_length=32,
                               error_messages={'required': u'لطفا نام کاربری خود را وارد کنید.'})
    password = forms.CharField(label=u'رمز عبور',widget=forms.PasswordInput)
    confirm_password = forms.CharField(label=u'تکرار رمز عبور',widget=forms.PasswordInput)
    email = forms.EmailField(label=u'رایانامه',
                             error_messages={
                                 'required': u'لطفا پست الکترونیکی خود را وارد کنید.',
                                 'invalid': u'لطفا یک آدرس ایمیل معتبر وارد کنید.',
                             })
    name = forms.CharField(label=u'نام و نام خانوادگی',max_length=40,
                           error_messages={'required': u'لطفا نام خود را وارد کنید.'})
    tel = forms.CharField(label=u'تلفن',max_length=16,
                            error_messages={'required': u'لطفا شماره تلفن خود را وارد کنید.'})
    address = forms.CharField(label=u'نشانی',max_length=120,widget=forms.Textarea(attrs={'cols':'27', 'rows':'3'}))

    def clean(self):
        if self.is_valid() is False:
            return None
        cleaned_data = super(CustomerSignUpForm, self).clean()
        if cleaned_data is False:
            return None
        password = cleaned_data['password']
        confirm = cleaned_data['confirm_password']
        if password != confirm:
            raise forms.ValidationError(u'رمز عبور با تکرار آن همخوانی ندارد.')
        tel = cleaned_data['tel']
        if tel.isdigit() is False:
            raise forms.ValidationError(u'شماره تلفن باید تماما رقم باشد.')

        return cleaned_data

from captcha.fields import CaptchaField

class LoginForm (forms.Form):

    username = forms.CharField(label=u'نام کاربری', max_length=40,
                               error_messages={'required':u'وارد کردن نام کاربری الزامی است.'})
    password = forms.CharField(label=u'رمز عبور', max_length=40, widget=forms.PasswordInput,
                               error_messages={'required':u'وارد کردن رمز عبور الزامی است.'})
    vendor_free = forms.BooleanField(widget=forms.HiddenInput,required=False)

    captcha = CaptchaField(label=u'Captcha',
                           error_messages={
                               'required': u'وارد کردن رمز تصویری اجباری است.',
                               'invalid': u'رمز تصویری اشتباه وارد شده است.',
                           })

    def clean(self):
        if self.is_valid() is False:
            return None
        cleaned_data = super(LoginForm, self).clean()
        return cleaned_data


class ChangePasswordForm (forms.Form):

    old_pass = forms.CharField(label=u'رمز عبور قدیمی', max_length=40, widget=forms.PasswordInput,
                               error_messages={'required': u'لطفا رمز عبور قدیمی خود را وارد کنید.'})
    new_pass = forms.CharField(label=u'رمز عبور جدید', max_length=40, widget=forms.PasswordInput,
                               error_messages={'required': u'لطفا رمز عبور جدید خود را وارد کنید.'})
    pass_conf = forms.CharField(label=u'تکرار رمز عبور', max_length=40, widget=forms.PasswordInput,
                                error_messages={'required': u'لطفا رمز عبور جدید خود را تکرار کنید.'})

    def clean(self):
        if self.is_valid() is False:
            return None
        cleaned_data = super(ChangePasswordForm,self).clean()
        if cleaned_data is False:
            return None
        new_pass = cleaned_data['new_pass']
        pass_conf = cleaned_data['pass_conf']
        if new_pass != pass_conf:
            raise forms.ValidationError(
                message=u'رمز عبور جدید با تکرار آن همخوانی ندارد.',
                code=u'invalid',
            )

        return cleaned_data


class CustomerInfoForm (forms.Form):

    username = forms.CharField(label=u'نام کاربری',max_length=32,
                               error_messages={'required': u'لطفا نام کاربری خود را وارد کنید.'})
    email = forms.EmailField(label=u'رایانامه',
                             error_messages={
                                 'required': u'لطفا پست الکترونیکی خود را وارد کنید.',
                                 'invalid':  u'لطفا یک آدرس ایمیل معتبر وارد کنید.',
                             })
    name = forms.CharField(label=u'نام و نام خانوادگی',max_length=40,
                           error_messages={'required': u'لطفا نام خود را وارد کنید.'})
    tel = forms.CharField(label=u'تلفن',max_length=16,
                            error_messages={'required': u'لطفا شماره تلفن خود را وارد کنید.'})
    address = forms.CharField(label=u'نشانی',max_length=120,widget=forms.Textarea(attrs={'cols':'27', 'rows':'3'}))

    def clean(self):
        if self.is_valid() is False:
            return None
        cleaned_data = super(CustomerInfoForm, self).clean()
        if cleaned_data is False:
            return None
        tel = cleaned_data['tel']
        if tel.isdigit() is False:
            raise forms.ValidationError(message=u'شماره تلفن باید تماما رقم باشد.', code='invalid')

        return cleaned_data