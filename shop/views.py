from django import forms
from django.contrib.auth.decorators import login_required
from django.contrib.sessions import serializers
from django.shortcuts import render,Http404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.utils import dateformat
from shop.models import Product, Category, User, Buy, Bill, Comment, Rate
import json
from math import ceil
from django.db.models import Q, Avg
from shop.forms import CustomerSignUpForm, LoginForm, ChangePasswordForm, CustomerInfoForm, DateSearchForm, ProductSearchForm, NewCatForm, RemoveCatForm
from django.contrib.auth import authenticate
from django.contrib.auth import login as log_in
from django.contrib.auth import logout as log_out
from datetime import datetime


def product(request, id):
    prod = Product.objects.get(id=id)
    cats = Category.objects.all()
    comments = Comment.objects.filter( Q(product=prod))
    newRate = Rate.objects.filter(product=Product.objects.get(id=id)).aggregate(Avg('rate'))['rate__avg']
    rateNum = Rate.objects.filter(product=Product.objects.get(id=id)).count()
    hasRated = False
    hisRate = 0
    if (not request.user.is_anonymous()) and (Rate.objects.filter(Q(owner=request.user)&Q(product=Product.objects.get(id=id))).count() > 0):
        hasRated = True
        hisRate=Rate.objects.filter(Q(owner=request.user)&Q(product=Product.objects.get(id=id)))[0].rate

    hasRated = Rate.objects.filter(product=Product.objects.get(id=id)).count()
    context = {'product': prod, 'categories': cats, 'comments': comments, 'rate': newRate, 'rateNum': rateNum, 'hasRated': hasRated, 'hisRate': hisRate}
    return render(request, 'product.html', context)


def index(request):
    cats = Category.objects.all()

    slides = Product.objects.filter(offPercent__gt=0)
    context = {'title': "صفحه اصلی", 'categories': cats, 'slides': slides, 'range': range(1,slides.count()),
               'favorites': Product.objects.order_by('-sold')[0:5], 'recents': Product.objects.order_by('-time')[0:5]
                }
    return render(request, 'index.html', context)

from shop.forms import ProductForm

@login_required
def addNewCategory(request):
    if request.user.status=="SHM":
        newcat = NewCatForm(request.GET)
        if(newcat.is_valid()):
            newcatform=newcat.save(commit=False)
            newcatform.save()

        return render(request,'shm_profile.html',shm_context(request, active=1, editCatMessage='دسته با موفقیت افزوده شد.'))
    return Http404()

@login_required
def selectCategory(request):
    if request.user.status=="SHM" and request.GET:
        form=RemoveCatForm(request.GET)
        if form.is_valid():
            cat = form.cleaned_data['cat']
            editCategoryForm = NewCatForm(instance=cat)
            return render(request,'shm_profile.html', shm_context(request, active=1, editCatForm=editCategoryForm, editCatID=cat.id))
        else:
            return render(request,'shm_profile.html', shm_context(request, active=1, editCatMessage='یک دسته را انتخاب کنید.'))
    return Http404()

@login_required
def editCategory(request, id):
    if request.user.status == "SHM" and request.GET:
        form = NewCatForm(request.GET)
        if form.is_valid():
            newcat = form.save(commit=False)
            cat = Category.objects.get(id=id)
            if cat.category_set.all().count() and newcat.sup_cat is not None:
                error='این دسته خود زیردسته دارد و نمی‌تواند زیردسته باشد.'
                return render(request,'shm_profile.html', shm_context(request, active=1, editCatForm=form, editCatID=id, editCatMessage=error))

            cat.name = newcat.name
            cat.sup_cat = newcat.sup_cat
            cat.save()
            return render(request,'shm_profile.html', shm_context(request, active=1, editCatMessage='دسته با موفقیت ویرایش شد.'))
        else:
            return render(request,'shm_profile.html', shm_context(request, active=1, editCatMessage='تغییرات ممکن نیست.'))
    return Http404()

@login_required
def removeCategory(request):
    error=''
    if request.user.status=="SHM":
        form=RemoveCatForm(request.GET)
        if form.is_valid():
            cat = form.cleaned_data['cat']
            if cat.product_set.all().count() > 0:
                error='به دلیل وجود محصول در دسته امکان حذف وجود ندارد.'
                print("products in category")
            elif cat.category_set.all().count() > 0:
                error='به دلیل وجود زیردسته امکان حذف وجود ندارد.'
                print("category has subcategories")
            else:
                error='دسته با موفقیت حذف شد.'
                Category.objects.get(id=cat.id).delete()


        return render(request,'shm_profile.html', shm_context(request, active=1, editCatMessage=error))

def shm_context(request, active=1, newprodform=ProductForm(), addNewProdSuccess=0, editCatMessage='', editCatForm='', editCatID=0):
    cats = Category.objects.all()
    prods = Product.objects.all()

    newcatform = NewCatForm()
    datebuys = []
    for bill in Bill.objects.all():
        for buy in bill.buy_set.all():
            datebuys.append(buy)

    for prod in prods:
        prod.num = len(Buy.objects.filter(product=prod))
        prod.buys =[]
        for buy in prod.buy_set.all():
            prod.buys.append(buy)
    return {'categories': cats, 'products': prods, 'allproducts': prods,
                                              'remove_cat_form': RemoveCatForm(),
                                              'new_cat_form': newcatform, 'new_prod_form': newprodform, 'date_buys': datebuys,
                                              'add_new_prod_success': addNewProdSuccess, 'active': active, 'edit_cat_message': editCatMessage,
                                              'edit_cat_form': editCatForm, 'edit_cat_id': editCatID}



def newProduct(request):
    form = ProductForm(request.POST, request.FILES)
    if form.is_valid():
        new_product = form.save(commit=False)
        new_product.availability = True
        new_product.save()
        return render(request, 'shm_profile.html', shm_context(request, active=2, addNewProdSuccess=1))
    else:
        return render(request, 'shm_profile.html', shm_context(request, active=2, newprodform=form, addNewProdSuccess=-1))

@login_required
def profile(request, edit_result=0, change_result=0):
    if request.user.status=="SHM":
        return render(request, 'shm_profile.html', shm_context(request, active=1))
    elif request.user.status=="CU":
        cats = Category.objects.all()
        title = u'صفحه شخصی'
        bills = request.user.bill_set.all().exclude(state=0)

        form = CustomerInfoForm(initial={
            'username': request.user.username,
            'email': request.user.email,
            'name': request.user.name,
            'tel': request.user.tel,
            'address': request.user.address,
        })

        password_form = ChangePasswordForm()

        context = {
            'categories': cats,
            'title': title,
            'bills': bills,
            'pass_form': password_form,
            'info_edit_form': form,
        }

        return render(request, 'customer_profile.html', context)
    elif request.user.status == "GM":
        sold = Product.objects.filter(buy__bill__state=3)
        total_bought = 0
        total_ordered = len(sold)
        for s in sold:
            print("{0} price {1}".format(s.name,s.real_value()))
            total_bought += s.real_value()
        print("SUM: {0}".format(total_bought))
        #information for products
        prod_list = Product.objects.all()
        best_sellers = sorted(prod_list, key=lambda x:x.total_bought(), reverse=True)
        most_popular = sorted(prod_list, key=lambda x:x.total_order(), reverse=True)
        best_sellers3 = best_sellers[0:3]
        most_popular3 = most_popular[0:3]
        best_sellers = best_sellers[3:]
        most_popular = most_popular[3:]
        #information for users
        user_num = len(User.objects.filter(status="CU"))
        user_list = User.objects.filter(status="CU")
        most_buy = sorted(user_list, key=lambda x:x.total_spent(), reverse=True)[0:20]
        most_order = sorted(user_list, key=lambda x:x.total_order(), reverse=True)[0:20]
        #information for warehouse
        total_stock = 0
        total_value = 0
        for prod in prod_list:
            total_stock += prod.number
            total_value += prod.number * prod.real_value()

        context = {
            'user_num': user_num,
            'wh_manager': User.objects.get(status="WM"),
            'sh_manager': User.objects.get(status="SHM"),
            'best_buy': most_buy,
            'most_order': most_order,
            'total_bought': total_bought,
            'total_ordered': total_ordered,
            'best_sellers3': best_sellers3,
            'most_popular3': most_popular3,
            'best_sellers': best_sellers,
            'most_popular': most_popular,
            'total_stock': total_stock,
            'total_value': total_value,
            'prods': prod_list,
            'title': "مدیریت کل"
        }
        return render(request, 'gm_profile.html', context)
    elif request.user.status == "WM":
        prods = Product.objects.all() #some products are deleted, but added again later
        pend_bills = Bill.objects.filter(state=1)
        for pend_bill in pend_bills:
            pend_bill.buys = []
            for buy in pend_bill.buy_set.all():
                pend_bill.buys.append(buy)
        sent_bills = Bill.objects.filter(state=2)
        for sent_bill in sent_bills:
            sent_bill.buys = []
            for buy in sent_bill.buy_set.all():
                sent_bill.buys.append(buy)
        rej_bills = list(Bill.objects.filter(state=4))
        for rb in rej_bills:
            rb.buys = []
            for buy in rb.buy_set.all():
                rb.buys.append(buy)
        context = {
            'title': "مدیریت انبار",
            'products': prods,
            'pend_bills': pend_bills,
            'sent_bills': sent_bills,
            'rej_bills': rej_bills,
        }
        return render(request, 'wm_profile.html', context)
    else:
        print ("this user is of {0} type".format(request.user.status))
        raise Http404()


@login_required
def get_user_info(request):
    if request.method == 'POST':
        return HttpResponseNotFound("Sorry. We couldn't find the page you've requested. Please try again later.")

    result = {}
    result['username'] = request.user.username
    result['email'] = request.user.email
    result['name'] = request.user.name
    result['address'] = request.user.address
    result['tel'] = request.user.tel

    return HttpResponse(json.dumps(result), content_type='aplication/json')

@login_required
def change_customer_password(request):
    if request.method == u'GET':
        profile(request)
    else:
        result = {}
        form = ChangePasswordForm(request.POST)
        if form.is_valid():
            print('Form is valid')
            data = form.cleaned_data
            if data is None:
                print("new password doesn't match with password confirmation")
                result['status'] = 'fail'
                result['message'] = u'رمز عبور جدید با تکرار آن همخوانی ندارد.'
                return HttpResponse(json.dumps(result), content_type='application/json')
            old_p = data['old_pass']
            new_p = data['new_pass']
            u = authenticate(username=request.user.username, password=old_p)
            if u is not None:
                print('The user is legit')
                u = User.objects.filter(pk=request.user.pk)[0]
                u.set_password(new_p)
                u.save()
                result['status'] = 'success'
                result['message'] = u'رمز عبور شما با موفقیت تغییر یافت.'
                return HttpResponse(json.dumps(result), content_type='application/json')
            else:
                print('The user did not authenticate')
                result['status'] = 'fail'
                result['message'] = u'رمز عبور کاربر نادرست است.'
                return HttpResponse(json.dumps(result), content_type='application/json')
        else:
            print('The form is not valid')
            result['status'] = 'fail'
            result['message'] = u'خطاهای زیر در فرم موجود است.'
            result['errors'] = form.errors
            print(form)
            return HttpResponse(json.dumps(result), content_type='application/json')

@login_required
def edit_customer_details(request):
    if request.method == u'GET':
        profile(request)
    else:
        result = {}
        form = CustomerInfoForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            name = form.cleaned_data['name']
            tel = form.cleaned_data['tel']
            address = form.cleaned_data['address']
            u = User.objects.filter(pk=request.user.pk)[0]
            u.username = username
            u.email = email
            u.name = name
            u.tel = tel
            u.address = address
            u.save()
            result['status'] = 'success'
            result['message'] = u'مشخصات کاربری شما با موفقیت تغییر یافت.'
            return HttpResponse(json.dumps(result), content_type='aplication/json')
        else:
            print('Invalid user info form')
            result['status'] = 'fail'
            result['message'] = u'خخطاهای زیر در ارسال فرم رخ داده است.'
            result['errors'] = form.errors
            return HttpResponse(json.dumps(result), content_type='aplication/json')



def products(request):
    cats = Category.objects.all()
    cat_id=1;
    if(request.GET.__contains__("cat-id")):
        cat_id = request.GET["cat-id"]
        context = {'categories': cats, 'cat_id': cat_id, 'query':""}
        return render(request, 'products.html', context)
    elif(request.GET.__contains__("query")):
        query = request.GET["query"]
        context = {'title': "نتایج جستجو", 'categories': cats, 'cat_id':"", 'query': query}
        return render(request, 'products.html', context)


def products_with_id(request, id):
    cats = Category.objects.all()
    context = {'title': Category.objects.get(id=id).__str__(), 'categories': cats, 'cat_id': id}

    return render(request, 'products.html', context)


def products_with_q(request, q):
    cats = Category.objects.all()
    context = {'title': "نتایج جستجو", 'categories': cats, 'query': q}

    return render(request, 'products.html', context)


def updateProductList(request):
    pagesize=6*2
    result = {'success': False}
    resProd = []
    if request.method != u'GET':
        return HttpResponse(json.dumps({}), content_type='application/json')
    GET = request.GET
    title = "";
    if GET.__contains__(u'cat') and GET.__contains__(u'page'):
        cat = int(GET[u'cat'])
        page = int(GET[u'page'])
        category = Category.objects.get(id=cat)
        title= category.__str__();
        subcats = category.category_set.all()
        prods = list(category.product_set.filter(availability=True))
        for category in subcats:
            prods.extend(list(category.product_set.filter(availability=True)))
    elif GET.__contains__(u'query') and GET.__contains__(u'page'):
        query = GET[u'query']
        page = int(GET[u'page'])
        title = "نتایج جستجو"
        prods = Product.objects.filter(availability=True).filter(Q(name__contains=query) |
                                       Q(category__name__contains=query) |
                                       Q(category__sup_cat__isnull=False,category__sup_cat__name__contains=query))

    l = len(prods)
    for i in range((page-1)*pagesize, min(page*pagesize, l)):
        curProd = {}
        curProd['name'] = prods[i].name
        curProd['price'] = int(prods[i].price)
        curProd['isOff'] = prods[i].offPercent>0
        curProd['realPrice'] = int(prods[i].real_value())
        curProd['id'] = prods[i].id
        curProd['url'] = prods[i].small_image.url
        curProd['rate'] = Rate.objects.filter(product=Product.objects.get(id=prods[i].id)).aggregate(Avg('rate'))['rate__avg']
        resProd.append(curProd)
    num = ceil(l/pagesize)
    result['products'] = resProd
    result['size'] = num
    result['title'] = title
    js = json.dumps(result)
    return HttpResponse(js, content_type='application/json')


def add_to_cart(request):
    prod_id = request.GET[u'id']
    prod = Product.objects.get(Q(id=prod_id) & Q(availability=True))
    bill = get_unpaid_cart(request)
    new_buy = Buy(bill=bill, product=prod)
    save_new_buy(request, new_buy)
    result = {'buy_id': new_buy.id}
    js = json.dumps(result)
    print(5)
    return HttpResponse(js, content_type='application/json')

@login_required
def add_comment(request):
    prod_id = request.GET[u'id']
    body = request.GET[u'body']
    print(prod_id, body)
    prod = Product.objects.get(id=prod_id)
    comment = Comment(owner=request.user, context=body, product=Product.objects.get(id=prod_id), time=datetime.now())
    comment.save()
    time = dateformat.format(datetime.now(), 'F j, Y, P')
    js = json.dumps({'name': request.user.name, 'time': time})
    return HttpResponse(js, content_type='application/json')


def remove_from_cart(request):
    buy_id = request.GET[u'buy_id']
    Buy.objects.get(id=buy_id).delete()
    js = json.dumps({})
    return HttpResponse(js, content_type='application/json')


def save_new_buy(request, nb):
    if not request.user.is_anonymous():
        nb.save()
    else:
        if request.session.has_key('cart'):
            pre_cart = request.session['cart']
            request.session['cart'] = pre_cart.append(nb)


def get_unpaid_cart(request):
    if not request.user.is_anonymous():
        print('getting unpaid car')
        cart = request.user.bill_set.get(state=0)
        print('getting unpaid car2')
    else:
        if request.session.has_key('cart'):
            cart = request.session['cart']
        else:
            cart = {}
    return cart


def load_cart(request):
    print("load cart")
    result = {}
    cart = get_unpaid_cart(request).buy_set.all()
    resBuys = []
    for buy in cart:
        curBuy = {}
        curBuy['name'] = buy.product.name
        curBuy['price'] = int(buy.product.price)
        curBuy['realPrice'] = int(buy.product.real_value())
        curBuy['prod_id'] = buy.product.id
        curBuy['buy_id'] = buy.id
        resBuys.append(curBuy)
    result['cart'] = resBuys
    js = json.dumps(result)

    return HttpResponse(js, content_type='application/json')

@login_required
def logout(request):
    log_out(request)
    if request.GET.get('next')!= "None":
        return HttpResponseRedirect(request.GET.get('next'))
    return index(request)


def login(request, customer_only=1):
    if not request.user.is_anonymous():
        log_out(request)
        return HttpResponseRedirect('/shop/login/?next='+(request.GET.get('next', '/shop/')))
    cats = Category.objects.all()
    signup_form = CustomerSignUpForm()
    login_form = LoginForm(initial={'vendor_free':customer_only})
    context = {'title': u"ورود کاربران", 'categories': cats, 'signup_form':signup_form, 'login_form':login_form, 'next':'', 'success': 0}

    if request.method == u'GET':
        next_path = request.GET.get('next', '/shop/')
        context['next'] = next_path
        return render(request, 'login.html', context)
    else:

        if request.POST.get('vendor_free', None) is None:
            #It's a sign up form
            form = CustomerSignUpForm(request.POST)
            if form.is_valid():
                User.objects.create_customer(form.cleaned_data['username'], form.cleaned_data['password'],
                                             form.cleaned_data['name'], form.cleaned_data['email'], form.cleaned_data['address'], form.cleaned_data['tel'])
                user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
                log_in(request, user)
                return HttpResponseRedirect(request.POST.get('next','/shop/'))


        else:
            #It's a login form
            form = LoginForm(request.POST)
            print('umadim too login')
            if form.is_valid():
                print('form valid boode')
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                print('User = {0} Pass = {1}'.format(username,password))
                user = authenticate(username=username,password=password)
                if user == request.user:
                    return HttpResponseRedirect('/shop/')
                if user is not None:
                    log_in(request, user)
                    print(request.POST)
                    print('Next sent with post is ' + str(request.POST.get('next')))
                    return HttpResponseRedirect(request.POST.get('next','/shop/'))
                else:
                    print('user or pass incorrect')
                    context['success'] = 2 # user or pass is incorrect
                    context['login_form'] = form
                    return render(request, 'login.html', context)
            else:
                print('invalid form')
                context['success'] = 1 # form is incorrect
                context['login_form'] = form
                return render(request, 'login.html', context)

            context = {'title': u"ورود کاربران", 'categories': cats, 'signup_form':signup_form, 'login_form':form}
            return render(request, 'login.html', context)


def undoRemoveProduct(request, id):
    prod = Product.objects.get(Q(pk=id))
    prod.availability = True
    prod.save()
    return render(request,'shm_profile.html',shm_context(request, active=3))


def removeProduct(request, id):
    prod = Product.objects.get(Q(pk=id))
    prod.availability = False
    prod.save()
    return render(request,'shm_profile.html',shm_context(request, active=3))


def editProduct(request, id):
    cats = Category.objects.all()
    title = u'ویرایش محصول'
    product = Product.objects.get(Q(pk=id))
    form = ProductForm(instance=product)

    context = {
        'title': title,
        'categories': cats,
        'product': product,
        'form': form,
    }

    if request.method == 'GET':
        return render(request, 'edit_product.html', context)
    else:
        form = ProductForm(request.POST, request.FILES)
        print(request.FILES)
        if form.is_valid():
            p = Product.objects.get(Q(pk=id) & Q(availability=True))
            newP = ProductForm(request.POST,request.FILES, instance=p)
            newP.time = datetime.now()
            newP.save()
            return HttpResponseRedirect('/shop/profile/')
        else:
            print('Invalid form')
            context['form'] = form
            return render(request,'edit_product.html', context)




@login_required
def billing(request, buy=False, empty_buy=False, shortage=False, shortages=0):
    unpaid_buys = get_unpaid_cart(request).buy_set.all()
    empty_cart = len(unpaid_buys) == 0
    sum = get_unpaid_cart(request).sum()

    context = {'categories': Category.objects.all(), 'empty_cart': empty_cart, 'empty_buy': empty_buy, 'buy': buy, 'prods': unpaid_buys, 'sum': sum,
               'shortage': shortage, 'shortages': shortages}
    return render(request, 'billing.html', context)

@login_required
def payment(request):
    cart = get_unpaid_cart(request)
    l = len(cart.buy_set.all())
    shortage = False
    shortages=[]
    if  l != 0:
        for prod in Product.objects.all():
            if cart.buy_set.filter(product=prod).count()>prod.number:
                shortage = True
                s = {'prod': prod, 'num': cart.buy_set.filter(product=prod).count()-prod.number}
                shortages.append(s)
        if not shortage:
            for buy in cart.buy_set.all():
                buy.product.number -= 1
                buy.product.save()
            cart.state = 1
            cart.time = datetime.now()
            cart.spent = cart.sum()
            print(cart.spent)
            cart.save()
            print("creating new bill")
            bill = Bill(state=0, customer=request.user)
            bill.save()
            print("new bill created")
    return billing(request, True, l == 0, shortage, shortages)

@login_required
def rateUpdate(request):
    prod = request.GET.get(u'prod')
    rate = request.GET.get(u'rate')
    print("rate is: "+ rate)
    if int(rate) == 0:
        print("delete sho dige")
        Rate.objects.filter(Q(owner=request.user) & Q(product=prod)).delete()
    else:
        if Rate.objects.filter(Q(owner=request.user) & Q(product=prod)).count() > 0:
            r = Rate.objects.filter(Q(owner=request.user) & Q(product=prod))[0]
            r.rate = rate
            r.save()
        else:
            r = Rate.objects.create(product=Product.objects.get(id=prod), owner = request.user, rate=int(rate))
            r.save()

    newRate = Rate.objects.filter(product=Product.objects.get(id=prod)).aggregate(Avg('rate'))['rate__avg']
    rateNum = Rate.objects.filter(product=Product.objects.get(id=prod)).count()
    print(newRate, rateNum)
    js = json.dumps({'newRate': newRate, 'rateNum': rateNum})
    return HttpResponse(js, content_type='application/json')


def customer_info(request):
    #TODO
    #returns customer info, good for ajax
    pass


def chert(request):
    return  render(request, 'chert.html')

def addRemoveProduct(request):
    prod_id = request.GET.get(u'pk')
    prod_num = request.GET.get(u'val')
    type = request.GET.get(u'type')
    if type == "add":
        num = int(prod_num)
        prod = Product.objects.filter(pk=prod_id)[0]
        initial_num = prod.number
        new_num = initial_num+num
        prod.number = new_num
        prod.save()
        js = json.dumps({'success':'true', 'new_num':new_num})
        return HttpResponse(js, content_type='application/json')
    elif type == "remove":
        num = int(prod_num)
        prod = Product.objects.filter(pk=prod_id)[0]
        initial_num = prod.number
        print('initial number is {0}'.format(initial_num))
        new_num = max(0,initial_num-num)
        prod.number = new_num
        prod.save()
        js = json.dumps({'success':'true', 'new_num':new_num})
        return HttpResponse(js, content_type='application/json')
    else:
        raise(Http404)

def billManagement(request):
    type = request.GET.get("type")
    if type == "accept":
        #We are accepting a bill
        pk = request.GET.get("id")
        bill = Bill.objects.filter(pk=pk)[0]
        bill.state = 2
        bill.save()
        js = json.dumps({'success':'true'})
        return HttpResponse(js, content_type='application/json')
    elif type == "receive":
        #Customer has received our product
        print("Customer received our product")
        pk = request.GET.get("id")
        bill = Bill.objects.filter(pk=pk)[0]
        bill.state = 3
        bill.save()
        js = json.dumps({'success':'true'})
        return HttpResponse(js, content_type='application/json')
    elif type == "reject":
        #Customer has sent back our product
        print("We are receiving sent-back product")
        pk = request.GET.get("id")
        bill = Bill.objects.get(pk=pk)
        for buy in bill.buy_set.all():
            buy.product.number+=1
            buy.product.save()
        bill.state = 4
        bill.save()
        js = json.dumps({'success':'true'})
        return HttpResponse(js, content_type='application/json')
    else:
        raise(Http404)

def user_with_id(request, id):
    user = User.objects.get(pk=id)
    cats = Category.objects.all()
    context = {
        'categories': cats,
        'title': u'مشخصات کاربری',
        'user': user,
    }
    return render(request, 'user.html', context)